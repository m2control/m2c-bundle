namespace MM.M2Control
{
	public static class CompanyDetails
	{
		public const string CompanyWeb = "https://www.verveconsulting.de/";
		public const string CompanySupport = "https://nextcloud.m2control.com";
		public const string Company = "Powered by Verve Consulting GmbH";
		public const string Copyright = "(C) 2012-19 M2Control";
		public const string Trademark = "";
	}
}